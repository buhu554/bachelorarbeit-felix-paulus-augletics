package de.ergo.backend.workout;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.crashlytics.android.answers.LoginEvent;
import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Timer;

import de.ergo.ErgoApplication;
import de.ergo.backend.hardware.HardwareSample;
import de.ergo.backend.hardware.config.HardwareConfig;
import de.ergo.backend.tools.LogCatcher;
import de.ergo.common.ButterworthForwardFilter;
import timber.log.Timber;

/**
 * Computes the boats speed based on a set of parameters and RowerSamples
 */
abstract public class BoatSimulator implements WorkoutSampleListener {
	public static final String TAG = "BoatSimulator";

	private ButterworthForwardFilter speedFilterForward = new ButterworthForwardFilter(167, 8.34f);
	private ButterworthForwardFilter speedFilterBackward = new ButterworthForwardFilter(167, 8.34f);

	private ButterworthForwardFilter accelerationFilterForward = new ButterworthForwardFilter(167, 8.34f);
	private ButterworthForwardFilter accelerationFilterBackward = new ButterworthForwardFilter(167, 8.34f);

	private ArrayList<Float> sampleTicks = new ArrayList<Float>();
	private ArrayList<Float> sampleSeconds = new ArrayList<Float>();
	private ArrayList<Float> extensionMax = new ArrayList<Float>();
	private ArrayList<Float> extensionMin = new ArrayList<Float>();

	private ArrayList<Float> sampleSpeeds = new ArrayList<Float>();
	private ArrayList<Float> sampleSpeedsFilteredForward = new ArrayList<Float>();
	private ArrayList<Float> sampleSpeedsFilteredBackward = new ArrayList<Float>();
	private ArrayList<Float> sampleSpeedsFiltered = new ArrayList<Float>();

	private ArrayList<Float> flywheelAccelerations = new ArrayList<Float>();
	private ArrayList<Float> flywheelAccelerationsFilteredForward = new ArrayList<Float>();
	private ArrayList<Float> flywheelAccelerationsFilteredBackward = new ArrayList<Float>();
	private ArrayList<Float> flywheelAccelerationsFiltered = new ArrayList<Float>();

	private int bValuesToAverage = 50;
	private float[] bValuesMemory = new float[bValuesToAverage];

	private int catchCount = 0;


	public float accelerationAtSpeed(float speed) {
		return (float)((-1)*Math.sqrt(631.517-4*2.421*(69.32-speed)));
	}


	@Override
	public void onCatch() {

		// checks

		if (!sampleSpeeds.isEmpty()) {
			Timber.tag("bValue").d("flywheelSpeeds.size: %d", sampleSpeeds.size());
			for (int j = 0; j < sampleSpeeds.size(); j++) {
				Timber.tag("continuousFlywheelSpeed").d("%f %f", sampleTicks.get(j), sampleSpeeds.get(j));
			}
		}
		if (sampleSpeeds.isEmpty() || sampleTicks.isEmpty() || sampleSpeeds.size() != sampleTicks.size()) {
			Timber.tag("bValue").d("Error: sampleTicks.size() != flywheelSpeeds.size() or one of both is empty.");
		}

		Timber.tag("sampleSpeedsArray").d("%s", sampleSpeeds.toString());


		// -- START --


		// 1) Geschwindigkeit vorwärts-rückwärts Filtern

		// a vorwärts
		for (int r = 0; r < sampleSpeeds.size(); r++) {
			sampleSpeedsFilteredForward.add(speedFilterForward.step(sampleSpeeds.get(r)));
		}

		// b rückwärts
		for (int w = sampleSpeedsFilteredForward.size()-1; w >= 0; w--) {
			sampleSpeedsFilteredBackward.add(speedFilterBackward.step(sampleSpeedsFilteredForward.get(w)));
		}

		// c Array flippen
		for (int b = sampleSpeedsFilteredBackward.size()-1; b >= 0 ; b--) {
			sampleSpeedsFiltered.add(sampleSpeedsFilteredBackward.get(b));
		}


		// 2) Geschwindigkeit bei Maximum beschneiden

		// a Maximum und Index von Maximum finden
		float maxSpeed = 0;
		int indexOfMaxSpeed = 0;
		for (int j = 0; j < sampleSpeedsFiltered.size(); j++) {
			if (sampleSpeedsFiltered.get(j) >= maxSpeed) {
				maxSpeed = sampleSpeedsFiltered.get(j);
				indexOfMaxSpeed = j;
			}
		}
		// b Beschneiden
		sampleSpeedsFiltered = new ArrayList<>(sampleSpeedsFiltered.subList(indexOfMaxSpeed, sampleSpeedsFiltered.size()-1));


		// 3) Ableiten

		int lastIndex = sampleSpeedsFiltered.size() - 1;
		float deltaT = 0.006f;

		// a erster Datenpunkt
		flywheelAccelerations.add((sampleSpeedsFiltered.get(1) - sampleSpeedsFiltered.get(0)) / deltaT);

		// b Datenpunkt 2 bis End-1
		for (int l = 1; l < lastIndex - 1; l++) {
			float speedGradient = (sampleSpeedsFiltered.get(l + 1) - sampleSpeedsFiltered.get(l - 1)) / 2;
			flywheelAccelerations.add(speedGradient / deltaT);
		}

		// c letzter Datenpunkt
		flywheelAccelerations.add((sampleSpeedsFiltered.get(lastIndex) - sampleSpeedsFiltered.get(lastIndex-1)) / deltaT);


		// 4) Beschleunigung vorwärts-rückwärts Filtern

		ArrayList<Float> front = new ArrayList<Float>();
		ArrayList<Float> end = new ArrayList<Float>();
		for (int q = 0; q <= 100; q++) {
			front.add(flywheelAccelerations.get(0));
			end.add(flywheelAccelerations.get(flywheelAccelerations.size()-1));
		}
		front.addAll(flywheelAccelerations);
		front.addAll(end);

		// a vorwärts
		for (int r = 0; r < flywheelAccelerations.size(); r++) {
			flywheelAccelerationsFilteredForward.add(accelerationFilterForward.step(flywheelAccelerations.get(r)));
		}

		// b rückwärts
		for (int w = flywheelAccelerationsFilteredForward.size()-1; w >= 0; w--) {
			flywheelAccelerationsFilteredBackward.add(accelerationFilterBackward.step(flywheelAccelerationsFilteredForward.get(w)));
		}

		// c Array flippen
		for (int b = flywheelAccelerationsFilteredBackward.size()-1; b >= 0 ; b--) {
			flywheelAccelerationsFiltered.add(flywheelAccelerationsFilteredBackward.get(b));
		}

		flywheelAccelerationsFiltered = new ArrayList<>(flywheelAccelerationsFiltered.subList(100, flywheelAccelerationsFiltered.size()-101));


		// 5) Beschleunigung am Minimum beschneiden (Wendepunkt der Geschwindigkeit)

		int centerIndex = (int) Math.floor(flywheelAccelerationsFiltered.size()/(float)2);

		// a Minimum von der Mitte aus finden (aka 'zeitlich spätestes Minimum in der ersten Hälfte')
		float minAcceleration = 0;
		int indexOfFirstMinAcceleration = centerIndex-1;

		for (int i = centerIndex; i >= 0; i--) {
			if (flywheelAccelerationsFiltered.get(i) <= minAcceleration) {
				minAcceleration = flywheelAccelerationsFiltered.get(i);
				indexOfFirstMinAcceleration = i;
			}
		}

		Timber.d("%d, %d, indexOfMin: %d", flywheelAccelerationsFiltered.size(), sampleSpeedsFiltered.size(), indexOfFirstMinAcceleration);

		// b an Minimum beschneiden
		flywheelAccelerationsFiltered = new ArrayList<>(flywheelAccelerationsFiltered.subList(indexOfFirstMinAcceleration, flywheelAccelerationsFiltered.size()-1));
		sampleSpeedsFiltered = new ArrayList<>(sampleSpeedsFiltered.subList(indexOfFirstMinAcceleration, sampleSpeedsFiltered.size()-1));

		Timber.d("%d, %d, indexOfMin: %d", flywheelAccelerationsFiltered.size(), sampleSpeedsFiltered.size(), indexOfFirstMinAcceleration);

		// die letzte 8% abziehen und beschneiden
		int indexOfMinusEightPercent = (int) Math.floor(flywheelAccelerationsFiltered.size() * 0.92);
		Timber.d("%d, %d, indexOfMinusEightPercent: %d", flywheelAccelerationsFiltered.size(), sampleSpeedsFiltered.size(), indexOfMinusEightPercent);

		flywheelAccelerationsFiltered = new ArrayList<>(flywheelAccelerationsFiltered.subList(0, indexOfMinusEightPercent));
		sampleSpeedsFiltered = new ArrayList<>(sampleSpeedsFiltered.subList(0, indexOfMinusEightPercent));

		Timber.d("%d, %d, indexOfMinusEightPercent: %d", flywheelAccelerationsFiltered.size(), sampleSpeedsFiltered.size(), indexOfMinusEightPercent);

		// 6) Projektion und Berechnung von bValue

		// a
		double n = 10;
		int divider = 0;
		int iterator = 1;
		int distance = (int) Math.floor(flywheelAccelerationsFiltered.size() / n);
		float bValueSum = 0;
		double[] bValueArray = new double[(int)n];

		for (int u = 1; u <= flywheelAccelerationsFiltered.size() - 1; u++) {
			if ((iterator % distance == 0) && (iterator/distance <= n)) {

				float speed = sampleSpeedsFiltered.get(u);
				float acceleration = flywheelAccelerationsFiltered.get(u);

				float accelerationReference = accelerationAtSpeed(speed);

				float bValue = accelerationReference - acceleration;

				if (bValue != 0.0f) {
					bValueArray[(iterator/distance) - 1] = bValue;
					bValueSum += bValue;
					divider++;
				}
			}
			iterator++;
		}




		float averageBValue = bValueSum /divider;
		Timber.tag("bValueOnly").d("%f", averageBValue);
		Timber.tag("bValueArray").d("%f, %d, %s", averageBValue, divider, Arrays.toString(bValueArray));
		Timber.tag("averageBValueSingle").d("%f, %d, %f", bValueSum, divider, averageBValue);


		// 7) Durchschnitt rückwirkend berechnen
		bValuesMemory[catchCount % bValuesToAverage] = averageBValue;

		float bValueSumOverTime = 0;
		for (int i = 0; i <= bValuesToAverage-1; i++) {
			bValueSumOverTime += bValuesMemory[i];
		}
		float bValueOverTime = bValueSumOverTime / bValuesToAverage;
		Timber.tag("averageBValueMultiple").d("%f", bValueOverTime);


		// 8) nächste Iteration vorbereiten

		sampleTicks.clear();
		sampleSeconds.clear();

		sampleSpeeds.clear();
		sampleSpeedsFiltered.clear();

		sampleSpeedsFilteredForward.clear();
		sampleSpeedsFilteredBackward.clear();

		flywheelAccelerations.clear();
		flywheelAccelerationsFiltered.clear();

		flywheelAccelerationsFilteredForward.clear();
		flywheelAccelerationsFilteredBackward.clear();

		accelerationFilterForward.reset();
		accelerationFilterBackward.reset();

		extensionMax.clear();
		extensionMin.clear();

		catchCount++;

	}

	public void onHardwareSampleFreqLvl1(HardwareSample.FreqLvl1 sample) {


		try {

			sampleTicks.add((float) sample.hardwareMicros);
			sampleSpeeds.add(sample.flywheelAngularSpeed);

		} catch (Exception any) {
			Timber.wtf(any);
		}
	}

}

